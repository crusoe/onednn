#!/bin/bash
set -e

cflags=$(dpkg-buildflags --get CFLAGS)
ldflags=$(dpkg-buildflags --get LDFLAGS)
libs="-ldnnl -lOpenCL"

compile_and_test () {
	local _cc=$1
	local _src=$2
	echo $_cc ${cflags[@]} ${ldflags[@]} $_src ${libs} -o tester
	$_cc ${cflags[@]} ${ldflags[@]} $2 ${libs} -o tester
	./tester
	rm tester || true
}

dump_test_command () {
	local _cc=$1
	local _src=$2
	cat >> debian/tests/control <<EOF
Test-Command: $_cc \$(dpkg-buildflags --get CFLAGS) \$(dpkg-buildflags --get LDFLAGS) $_src ${libs} -o tester; ./tester
Depends: gcc, g++, clang, libc6-dev, libdnnl-dev, ocl-icd-opencl-dev,
Restrictions: allow-stderr

EOF
}

tests=(
examples/cnn_inference_f32.c
examples/cnn_inference_f32.cpp
examples/cnn_inference_int8.cpp
examples/cnn_training_bf16.cpp
examples/cnn_training_f32.cpp
examples/cpu_cnn_training_f32.c
examples/cpu_rnn_inference_f32.cpp
examples/cpu_rnn_inference_int8.cpp
#examples/cross_engine_reorder.c
#examples/cross_engine_reorder.cpp
examples/getting_started.cpp
#examples/gpu_opencl_interop.cpp
examples/memory_format_propagation.cpp
examples/performance_profiling.cpp
examples/rnn_training_f32.cpp
#examples/sycl_interop.cpp
#examples/sycl_interop_usm.cpp
)

case "$1" in
	test)
		for compiler in g++ clang++; do
			for t in ${tests[@]}; do
				compile_and_test $compiler $t
			done
		done
		;;
	generate)
		truncate -s0 debian/tests/control
		for compiler in g++ clang++; do
			for t in ${tests[@]}; do
				dump_test_command $compiler $t
			done
		done
		;;
	*)
		echo ???
		;;
esac
